//
// Created by moyak on 03.11.2022.
//
/*  deserializer   */

#include "bmp.h"
#include "image.h"

#include <stdbool.h>
#define BFTYPE 19778
#define BFREZERVED 0
#define BISIZE 40
#define OFFSET 54  
#define HEADER_SIZE sizeof(struct bmp_header)
#define PLANES 1
#define BITS 24
#define COMPRESSION 0
#define PIXELS_PER_METER 2835
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

uint32_t bytes_to_div(uint32_t width) {
    uint32_t mod = (4 - width * PIXEL_SIZE % 4) % 4;

    return mod;
}



enum read_status from_bmp( FILE* in, struct image* img) {
    struct bmp_header header = {0};
    size_t c = fread(&header, sizeof( struct bmp_header ), 1, in );
    if(c != 1) {
        return READ_INVALID_HEADER;
    }
    if(header.bfType != BFTYPE) return READ_INVALID_SIGNATURE;
    size_t width = header.biWidth;
    size_t height = header.biHeight;
    struct pixel* pixelsBegging = malloc_image_data(width, height);
    
    for(size_t i = 0; i < height; i++) {
        size_t d = fread( pixelsBegging + i * width, sizeof( struct pixel), width, in );
        if(d!=width) return READ_INVALID_BITS;
        uint32_t mod = bytes_to_div(width);
        if(fseek( in, mod, SEEK_CUR ) != 0) {
            return READ_INVALID_BITS;
        };
    }
    img->data = pixelsBegging;
    img->height = height;
    img->width = width;
    return READ_OK;
}


size_t get_size(struct image const* img) {
    return (img->width)*(img->height) + bytes_to_div(img->width)*(img->height);
}


enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = {0};
    header.bfType = BFTYPE;
    header.bfReserved = BFREZERVED;
    header.bOffBits = OFFSET;
    header.biSize = BISIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biBitCount = BITS;
    header.biCompression = COMPRESSION;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = PIXELS_PER_METER;
    header.biYPelsPerMeter = PIXELS_PER_METER;
    header.biClrUsed = COLORS_USED;
    header.biClrImportant = COLORS_IMPORTANT;
    header.biSizeImage = get_size(img);
    header.bfileSize = header.biSizeImage + HEADER_SIZE;
    size_t c = fwrite(&header, HEADER_SIZE, 1, out);
    if(c!=1) return WRITE_ERROR;
    for(int i = 0; i < img->height; i++) {
        size_t cc = fwrite(img->data + i * (img -> width), PIXEL_SIZE, img->width, out);
        if(cc< img -> width) {
            return WRITE_ERROR;
        }

        int8_t randomBytes[] = {0, 0, 0, 0};

        size_t mod = bytes_to_div(img->width);
        fwrite(randomBytes, 1, mod, out);
    }
    return WRITE_OK;
}
