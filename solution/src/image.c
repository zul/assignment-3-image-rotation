#include "image.h"
#include "malloc.h"
struct pixel* malloc_image_data (uint64_t width, uint64_t height) {
    return (struct pixel*) malloc(sizeof(struct pixel)*width*height);
    
}
void destroy_img(struct image* img) {
    free(img->data);
}
