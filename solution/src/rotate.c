#include "rotate.h"
#include <stddef.h>

struct image rotate( struct image const source ) {
    struct image image = {0};
    image.data = malloc_image_data(source.height, source.width);

    uint64_t iter = 0;
    for(uint32_t i = 0; i <= source.width - 1; i++) {
        for(uint32_t j = source.height-1; j > 0; j--) {
            
            image.data[iter] = source.data[i + j * source.width];
            iter++;
        }

        image.data[iter] = source.data[i];
        iter++;
    }
    image.height = source.width;
    image.width = source.height;        
    return image;
}
