#include "bmp.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc < 3) {
        printf("%s", "Not enough params");
        return 0;
    }
    FILE* in = fopen(argv[1], "rb");
   
    if(in == NULL) {
        printf("%s", "No such file");
        return 0;
    }
    struct image img = {0};

    enum read_status st =  from_bmp(in, &img);
    if(st != READ_OK) {
        printf("%s", "something happened:(");
        destroy_img(&img);
        return 0;
    }
    fclose(in);

    struct image img_new = {0};
    img_new = rotate(img);
    destroy_img(&img);


    FILE* out = fopen(argv[2], "wb");
    if(out == NULL) {
        printf("%s", "No such file");
        destroy_img(&img_new);
        return 0;
    }
    to_bmp(out, &img_new);

    if(fclose(out) != 0) {
        printf("%s", "Cannot close file");
    }
    destroy_img(&img_new);

    return 0;
}
