
#ifndef INC_3LAB_IMAGE_H
#define INC_3LAB_IMAGE_H

#include <stdint.h>
#define PIXEL_SIZE 3

struct __attribute__ ((__packed__)) image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__ ((__packed__)) pixel { uint8_t b, g, r; };

struct pixel* malloc_image_data(uint64_t width, uint64_t height); //return array of pixels
void destroy_img(struct image* img);
#endif 

